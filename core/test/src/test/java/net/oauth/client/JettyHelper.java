package net.oauth.client;

import java.io.IOException;
import java.net.Socket;

public class JettyHelper  {

    public static int getRandomSocket() throws IOException {
        // Get an ephemeral local port number:
        Socket s = new Socket();
        s.bind(null);
        int port = s.getLocalPort();
        s.close();
        return port;
    }

}
